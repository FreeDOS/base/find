# find

Display all lines in one or more files that contain a given string. Inverse and case-insensitive search possible as well.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## FIND.LSM

<table>
<tr><td>title</td><td>find</td></tr>
<tr><td>version</td><td>3.0b LFN (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2019-07-03</td></tr>
<tr><td>description</td><td>Search for files that contain a string</td></tr>
<tr><td>summary</td><td>Display all lines in one or more files that contain a given string. Inverse and case-insensitive search possible as well.</td></tr>
<tr><td>keywords</td><td>freedos, find, grep</td></tr>
<tr><td>author</td><td>Jim Hall &lt;jhall -at- freedos.org&gt;</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/</td></tr>
<tr><td>platforms</td><td>DOS (e.g. Borland C, Turbo C), includes own KITTEN library version</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Find</td></tr>
</table>
